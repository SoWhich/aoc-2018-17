﻿namespace aoc._2018._17

open Elmish
open Avalonia
open Avalonia.Controls.ApplicationLifetimes
open Avalonia.Diagnostics
open Avalonia.Input
open Avalonia.FuncUI
open Avalonia.FuncUI.Elmish
open Avalonia.Themes.Fluent
open Avalonia.FuncUI.Hosts

open Serilog

type MainWindow() as this =
    inherit HostWindow()

    do
        base.Title <- "aoc._2018._17"
        base.Width <- 400.0
        base.Height <- 400.0

        //this.VisualRoot.VisualRoot.Renderer.DrawFps <- true
        //this.VisualRoot.VisualRoot.Renderer.DrawDirtyRects <- true
        this.AttachDevTools()

        Elmish.Program.mkProgram (fun () -> Fluid.init, Cmd.ofMsg (Fluid.Reset "res/test1")) Fluid.update Fluid.view
        |> Program.withHost this
        |> Program.run


type App() =
    inherit Application()

    override this.Initialize() =
        this.Styles.Add(FluentTheme(baseUri = null, Mode = FluentThemeMode.Dark))

    override this.OnFrameworkInitializationCompleted() =
        match this.ApplicationLifetime with
        | :? IClassicDesktopStyleApplicationLifetime as desktopLifetime -> desktopLifetime.MainWindow <- MainWindow()
        | _ -> ()

module Program =
    let traceListener unit =
        new System.Diagnostics.TextWriterTraceListener(System.Console.OpenStandardOutput())

    [<EntryPoint>]
    let main (args: string[]) =
        Log.Logger <- LoggerConfiguration().Destructure.FSharpTypes().WriteTo.Trace().CreateLogger()

        System.Diagnostics.Trace.Listeners.Add(traceListener ()) |> ignore

        AppBuilder
            .Configure<App>()
            .UsePlatformDetect()
            .UseSkia()
            .LogToTrace(Logging.LogEventLevel.Information)
            .StartWithClassicDesktopLifetime(args)
