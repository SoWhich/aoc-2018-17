namespace aoc._2018._17

open Serilog

module Parser =
    type Point = WetTree.Point

    type Dimension =
        | X
        | Y

    let extract_single (str: string) =
        let parts = str.Split "="
        let num = int (parts.[1])

        match parts.[0] with
        | "x" -> (X, num)
        | "y" -> (Y, num)
        | _ -> raise (MatchFailureException("Got non x or y coord", 0, 0))

    let extract_range (str: string) =
        let parts = str.Split "="
        let nums = parts.[1].Split ".."
        let lower = int nums.[0]
        let upper = int nums.[1]

        match parts.[0] with
        | "x" -> (X, lower, upper)
        | "y" -> (Y, lower, upper)
        | _ -> raise (MatchFailureException("Got non x or y coord", 0, 0))

    let load (lines: string seq) : Point seq =
        seq {
            for line in lines do
                let parts = line.Split ", "
                let (sname, sval) = extract_single parts.[0]
                let (mname, lower, upper) = extract_range parts.[1]

                for mval in lower..upper do
                    yield
                        match sname, mname with
                        | X, Y -> { x = sval; y = mval }
                        | Y, X -> { y = sval; x = mval }
                        | _ -> raise (MatchFailureException("got double of x or y in line", 0, 0))
        }

    let loadFile (path: string) =
        Log.Debug("Loading file {@string}", path)
        let points = path |> System.IO.File.ReadLines |> load
        Log.Debug("Loaded")
        points
