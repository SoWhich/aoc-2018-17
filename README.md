
# aoc._2018._17

This is an Avalonia.FuncUI Starter template, this template shows you in a brief way how you can create components and functions to render your UI.

To run this application just type

```
dotnet run
```

You should briefly see your application window showing on your desktop.

a solution attempt/visualization of [this advent of code problem](https://adventofcode.com/2018/day/17)

Currently gets the wrong answer
