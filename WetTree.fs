namespace aoc._2018._17

open Serilog

module WetTree =

    type Point = { x: int; y: int }

    type Fill =
        | Clay
        | Water

    type Span = { left: int; right: int; y: int }

    type State =
        { board: Map<Point, Fill>
          active: list<Span>
          inactive: Set<Span> }

    let Left span = { x = span.left; y = span.y }
    let Right span = { x = span.right; y = span.y }

    let Point span =
        if span.right = span.left then Some(Left span) else None

    let ofPoint pt = { left = pt.x; right = pt.x; y = pt.y }

    let toPoints span =
        seq { for x in span.left .. span.right -> { x = x; y = span.y } }

    type __Prec =
        { up: Point -> Point
          down: Point -> Point
          left: Point -> Point
          right: Point -> Point }

    let point =
        { up = fun pt -> { pt with y = pt.y - 1 }
          down = fun pt -> { pt with y = pt.y + 1 }
          left = fun pt -> { pt with x = pt.x - 1 }
          right = fun pt -> { pt with x = pt.x + 1 } }

    let Edge span dir = span |> dir |> ofPoint

    let init =
        { board = Map.empty
          inactive = Set.empty
          active = [ { left = 500; right = 500; y = 0 } ] }

    let fill clay =
        clay |> Seq.map (fun c -> (c, Clay)) |> Map.ofSeq

    let addWater board span =
        let paired =
            seq {
                yield! Map.toSeq board
                yield! Seq.zip (toPoints span) (Seq.replicate (span.right - span.left + 1) Water)
            }

        Map.ofSeq paired

    let countReached state =
        let reached =
            seq {
                yield!
                    state.board
                    |> Map.toSeq
                    |> Seq.filter (fun (_, v) -> v = Water)
                    |> Seq.map (fun (k, _) -> k)

                yield! Seq.map toPoints state.active |> Seq.concat
                yield! Seq.map toPoints state.inactive |> Seq.concat
            }

        reached |> Set.ofSeq |> Set.count


    let stopped board (pt: Point) =
        Map.containsKey pt board || Map.containsKey (point.down pt) board

    let bounded board (span: Span) =
        let left = Left span
        let right = Right span

        stopped board left
        && stopped board right
        && Map.containsKey (point.left left) board
        && Map.containsKey (point.right right) board

    let maximize board span =
        let rec grow board span =
            let left = Left span
            let lb = point.down left
            let right = Right span
            let rb = point.down right

            let growable_left =
                stopped board left && not (Map.containsKey (point.left left) board)

            let growable_right =
                stopped board right && not (Map.containsKey (point.right right) board)

            if growable_left then
                grow board { span with left = span.left - 1 }
            else if growable_right then
                grow board { span with right = span.right + 1 }
            else
                span

        grow board span

    let rec step board tree =
        match Point tree with
        | Some pt ->
            let beneath = point.down pt

            if Map.containsKey pt board then
                Set.empty
            else if stopped board pt then
                let max = maximize board (ofPoint pt)

                if max = ofPoint pt then Set.empty else Set.singleton max
            else
                Set.singleton (ofPoint beneath)
        | None ->
            let sideSet dir =
                let side = dir tree

                let tree = ofPoint side

                let treeSet = if stopped board side then Set.empty else step board tree
                treeSet

            Set.union (sideSet Left) (sideSet Right)


    let iterate state dims =
        let active = List.tryHead state.active

        match active with
        | Some current ->
            let res =
                step state.board current
                |> Set.toSeq
                |> Seq.filter (fun span -> span.y <= dims.y)
                |> Seq.filter (fun span -> not (Set.contains span state.inactive))
                |> List.ofSeq

            let board =
                if bounded state.board current then
                    addWater state.board current
                else
                    state.board

            let active =
                if not (List.isEmpty res) then
                    List.concat (
                        seq {
                            yield res
                            yield state.active
                        }
                    )
                else
                    List.tail state.active

            let inactive = current |> Set.singleton |> Set.union state.inactive

            { board = board
              active = active
              inactive = inactive }

        | None -> state


    let rec complete state dims =
        if List.isEmpty state.active then
            state
        else
            complete (iterate state dims) dims
