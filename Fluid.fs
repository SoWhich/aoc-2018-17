﻿namespace aoc._2018._17


module Fluid =
    open Elmish
    open Avalonia.Controls
    open Avalonia.FuncUI.DSL
    open Avalonia.Layout
    open Avalonia.Media
    open Avalonia.Media.Immutable
    open Serilog

    type Point = WetTree.Point
    type Span = WetTree.Span

    type State =
        { tree: WetTree.State
          dims: Span
          boxSize: double
          loading: bool }

    let init =
        { tree = WetTree.init
          dims = { left = 0; right = 0; y = 0 }
          boxSize = 5.0
          loading = false }


    let dims (board: Map<Point, WetTree.Fill>) : Span =
        let points: Point seq =
            seq {
                yield { x = 500; y = 1 }
                yield! Map.toSeq board |> Seq.map (fun (k, _) -> k)
            }

        let left = Seq.minBy (fun (k: Point) -> k.x) points
        let right = Seq.maxBy (fun (k: Point) -> k.x) points
        let height = Seq.maxBy (fun (k: Point) -> k.y) points

        { left = left.x - 1
          right = right.x + 1
          y = height.y }

    type Msg =
        | Iterate
        | Complete
        | Grow
        | Shrink
        | Reset of string

    let update (msg: Msg) state =
        match msg with
        | Iterate -> { state with tree = WetTree.iterate state.tree state.dims }, Cmd.none
        | Complete -> { state with tree = WetTree.complete state.tree state.dims }, Cmd.none
        | Grow -> { state with boxSize = state.boxSize * 1.5 }, Cmd.none
        | Shrink -> { state with boxSize = state.boxSize / 1.5 }, Cmd.none
        | Reset source ->
            let clay = Parser.loadFile source
            let board = WetTree.fill clay
            let tree = { WetTree.init with board = board }
            let dims = dims board

            { tree = tree
              dims = dims
              boxSize = init.boxSize
              loading = false },
            Cmd.none

    let pointToColor pt wet occupied =
        match Map.tryFind pt occupied with
        | Some WetTree.Clay -> Colors.Brown
        | Some WetTree.Water -> Colors.Blue
        | None ->
            if Set.contains pt wet then
                Colors.SkyBlue
            else
                Colors.Transparent

    let colorToBrush (color: Color) = new ImmutableSolidColorBrush(color)


    let pointToRect boxSize (dims: Span) wet occupied (pt: Point) : Avalonia.FuncUI.Types.IView =
        let x = float (pt.x - dims.left) * boxSize
        let y = float (pt.y) * boxSize

        Rectangle.create
            [ Shapes.Shape.left (x)
              Shapes.Shape.top (y)
              Shapes.Shape.width (boxSize)
              Shapes.Shape.height (boxSize)
              Shapes.Shape.fill (new ImmutableSolidColorBrush(pointToColor pt wet occupied)) ]


    let view (state: State) (dispatch) : Avalonia.FuncUI.Types.IView =
        if state.loading then
            TextBlock.create [ TextBlock.text "Loading" ]
        else
            let wet =
                Set.ofSeq (
                    seq {
                        for span in state.tree.active do
                            yield! WetTree.toPoints span

                        for span in state.tree.inactive do
                            yield! WetTree.toPoints span
                    }
                )

            let points =
                seq {
                    yield! wet
                    yield! state.tree.board.Keys
                }

            let pmapf = pointToRect state.boxSize state.dims wet state.tree.board

            StackPanel.create
                [ StackPanel.orientation Orientation.Vertical
                  StackPanel.children
                      [ DockPanel.create
                            [ DockPanel.children
                                  [ if List.isEmpty state.tree.active then
                                        TextBlock.create
                                            [ TextBlock.text (
                                                  String.concat
                                                      " "
                                                      [ "Finished with count"
                                                        string (WetTree.countReached state.tree) ]
                                              ) ]
                                    Button.create
                                        [ Button.dock Dock.Top
                                          Button.onClick (fun _ -> dispatch Grow)
                                          Button.content "+" ]

                                    Button.create
                                        [ Button.dock Dock.Bottom
                                          Button.onClick (fun _ -> dispatch Shrink)
                                          Button.content "-" ]

                                    Button.create
                                        [ Button.dock Dock.Left
                                          Button.onClick (fun _ -> dispatch (Reset "res/test1"))
                                          Button.content "o" ]

                                    Button.create
                                        [ Button.dock Dock.Right
                                          Button.onClick (fun _ -> dispatch Iterate)
                                          Button.content ">" ]

                                    Button.create
                                        [ Button.dock Dock.Right
                                          Button.onClick (fun _ -> dispatch Complete)
                                          Button.content "" ] ] ]
                        Canvas.create
                            [ Canvas.width (float (state.dims.right - state.dims.right + 1) * state.boxSize)
                              Canvas.height (float (state.dims.y) * state.boxSize)
                              Canvas.background (colorToBrush (Colors.White))
                              Canvas.children (points |> Seq.map pmapf |> List.ofSeq) ] ] ]
